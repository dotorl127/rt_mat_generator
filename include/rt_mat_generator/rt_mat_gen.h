#ifndef RT_MAT_GEN_H
#define RT_MAT_GEN_H

#include "rt_mat_generator_ros.h"
#include <QMainWindow>

namespace Ui
{
class rt_mat_gen;
}

class rt_mat_gen : public QMainWindow
{
  Q_OBJECT

public:
  explicit rt_mat_gen(int argc, char** argv, QWidget* parent = 0);
  ~rt_mat_gen();

public Q_SLOTS:
  void on_x_lineEdit_textEdited(const QString &x);
  void on_y_lineEdit_textEdited(const QString &y);
  void on_z_lineEdit_textEdited(const QString &z);

  void on_roll_lineEdit_textEdited(const QString &roll);
  void on_pitch_lineEdit_textEdited(const QString &pitch);
  void on_yaw_lineEdit_textEdited(const QString &yaw);

  void on_roll_horizontalSlider_sliderMoved(int roll_position);
  void on_pitch_horizontalSlider_sliderMoved(int pitch_position);
  void on_yaw_horizontalSlider_sliderMoved(int yaw_position);

  void on_rt_generate_button_clicked();

private:
  Ui::rt_mat_gen* ui;
  rt_mat_generator_ui::rt_mat_generator* ros_node;
};

#endif // RT_MAT_GEN_H
