#ifndef RT_MAT_GENERATOR_ROS_H
#define RT_MAT_GENERATOR_ROS_H

#ifndef Q_MOC_RUN
// RT_MAT_GENERATOR_ROS

#include <ros/node_handle.h>
#include <ros/ros.h>
#include <keti_msgs/ProjectedPoints.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/CameraInfo.h>

#include <pcl_ros/transforms.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <tf/tf.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <QThread>
#include <string>
#endif

namespace rt_mat_generator_ui
{

class rt_mat_generator : public QThread
{
  Q_OBJECT

public:
  rt_mat_generator(int argc, char** argv);
  virtual ~rt_mat_generator();
  bool init();
  void init_nh();
  void run();

  void pointcloudCallback(const sensor_msgs::PointCloud2::ConstPtr &points);
  void camerainfoCallback(const sensor_msgs::CameraInfo::ConstPtr &cam_info);

  pcl::PointXYZ TransformPoint(const pcl::PointXYZI &in_point, const tf::StampedTransform &in_transform);
  tf::StampedTransform FindTransform();

  float init_x_, init_y_, init_z_;
  float roll_, pitch_, yaw_;
  float fx_, fy_, cx_, cy_;
  QString final_rt_mat_;
  cv::Size image_size_;
  tf::StampedTransform camera_lidar_tf_;

  bool camera_info_ok_;

Q_SIGNALS:
  void rosShutdown();

private:
  double origin_mat[12] = {1, 0, 0, 0,
                           0, 1, 0, 0,
                           0, 0, 1, 0};
  int init_argc;
  char** init_argv;
  ros::NodeHandle* nh;
  ros::Subscriber point_cloud_sub_, camera_info_sub_;
  ros::Publisher projected_point_pub_;
  bool isConnected;
};
} // namespace rt_mat_generator_ui

#endif // RT_MAT_GENERATOR_ROS_H
