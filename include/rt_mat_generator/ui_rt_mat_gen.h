/********************************************************************************
** Form generated from reading UI file 'rt_mat_gen.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RT_MAT_GEN_H
#define UI_RT_MAT_GEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_rt_mat_gen
{
public:
  QWidget* centralWidget;
  QLabel* roll_label;
  QLabel* pitch_label;
  QLabel* yaw_label;
  QSlider* roll_horizontalSlider;
  QSlider* pitch_horizontalSlider;
  QSlider* yaw_horizontalSlider;
  QLineEdit* roll_lineEdit;
  QLineEdit* pitch_lineEdit;
  QLineEdit* yaw_lineEdit;
  QLineEdit* rt_result_lineEdit;
  QLabel* x_label;
  QTextEdit* x_textEdit;
  QTextEdit* y_textEdit;
  QLabel* y_label;
  QTextEdit* z_textEdit;
  QLabel* z_label;
  QMenuBar* menuBar;
  QToolBar* mainToolBar;
  QStatusBar* statusBar;

  void setupUi(QMainWindow* rt_mat_gen)
  {
    if (rt_mat_gen->objectName().isEmpty())
      rt_mat_gen->setObjectName(QStringLiteral("rt_mat_gen"));
    rt_mat_gen->resize(461, 467);
    centralWidget = new QWidget(rt_mat_gen);
    centralWidget->setObjectName(QStringLiteral("centralWidget"));
    roll_label = new QLabel(centralWidget);
    roll_label->setObjectName(QStringLiteral("roll_label"));
    roll_label->setGeometry(QRect(20, 70, 41, 31));
    pitch_label = new QLabel(centralWidget);
    pitch_label->setObjectName(QStringLiteral("pitch_label"));
    pitch_label->setGeometry(QRect(20, 130, 41, 31));
    yaw_label = new QLabel(centralWidget);
    yaw_label->setObjectName(QStringLiteral("yaw_label"));
    yaw_label->setGeometry(QRect(20, 190, 41, 31));
    roll_horizontalSlider = new QSlider(centralWidget);
    roll_horizontalSlider->setObjectName(
        QStringLiteral("roll_horizontalSlider"));
    roll_horizontalSlider->setGeometry(QRect(70, 75, 271, 21));
    roll_horizontalSlider->setMinimum(-180);
    roll_horizontalSlider->setMaximum(180);
    roll_horizontalSlider->setOrientation(Qt::Horizontal);
    pitch_horizontalSlider = new QSlider(centralWidget);
    pitch_horizontalSlider->setObjectName(
        QStringLiteral("pitch_horizontalSlider"));
    pitch_horizontalSlider->setGeometry(QRect(70, 135, 271, 21));
    pitch_horizontalSlider->setMinimum(-180);
    pitch_horizontalSlider->setMaximum(180);
    pitch_horizontalSlider->setOrientation(Qt::Horizontal);
    yaw_horizontalSlider = new QSlider(centralWidget);
    yaw_horizontalSlider->setObjectName(QStringLiteral("yaw_horizontalSlider"));
    yaw_horizontalSlider->setGeometry(QRect(70, 195, 271, 21));
    yaw_horizontalSlider->setMinimum(-180);
    yaw_horizontalSlider->setMaximum(180);
    yaw_horizontalSlider->setOrientation(Qt::Horizontal);
    roll_lineEdit = new QLineEdit(centralWidget);
    roll_lineEdit->setObjectName(QStringLiteral("roll_lineEdit"));
    roll_lineEdit->setGeometry(QRect(360, 70, 81, 31));
    roll_lineEdit->setAlignment(Qt::AlignCenter);
    pitch_lineEdit = new QLineEdit(centralWidget);
    pitch_lineEdit->setObjectName(QStringLiteral("pitch_lineEdit"));
    pitch_lineEdit->setGeometry(QRect(360, 130, 81, 31));
    pitch_lineEdit->setAlignment(Qt::AlignCenter);
    yaw_lineEdit = new QLineEdit(centralWidget);
    yaw_lineEdit->setObjectName(QStringLiteral("yaw_lineEdit"));
    yaw_lineEdit->setGeometry(QRect(360, 190, 81, 31));
    yaw_lineEdit->setAlignment(Qt::AlignCenter);
    rt_result_lineEdit = new QLineEdit(centralWidget);
    rt_result_lineEdit->setObjectName(QStringLiteral("rt_result_lineEdit"));
    rt_result_lineEdit->setGeometry(QRect(20, 250, 421, 151));
    x_label = new QLabel(centralWidget);
    x_label->setObjectName(QStringLiteral("x_label"));
    x_label->setGeometry(QRect(20, 20, 31, 31));
    x_textEdit = new QTextEdit(centralWidget);
    x_textEdit->setObjectName(QStringLiteral("x_textEdit"));
    x_textEdit->setGeometry(QRect(50, 20, 81, 31));
    y_textEdit = new QTextEdit(centralWidget);
    y_textEdit->setObjectName(QStringLiteral("y_textEdit"));
    y_textEdit->setGeometry(QRect(200, 20, 81, 31));
    y_label = new QLabel(centralWidget);
    y_label->setObjectName(QStringLiteral("y_label"));
    y_label->setGeometry(QRect(170, 20, 31, 31));
    z_textEdit = new QTextEdit(centralWidget);
    z_textEdit->setObjectName(QStringLiteral("z_textEdit"));
    z_textEdit->setGeometry(QRect(360, 20, 81, 31));
    z_label = new QLabel(centralWidget);
    z_label->setObjectName(QStringLiteral("z_label"));
    z_label->setGeometry(QRect(330, 20, 31, 31));
    rt_mat_gen->setCentralWidget(centralWidget);
    menuBar = new QMenuBar(rt_mat_gen);
    menuBar->setObjectName(QStringLiteral("menuBar"));
    menuBar->setGeometry(QRect(0, 0, 461, 22));
    rt_mat_gen->setMenuBar(menuBar);
    mainToolBar = new QToolBar(rt_mat_gen);
    mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
    rt_mat_gen->addToolBar(Qt::TopToolBarArea, mainToolBar);
    statusBar = new QStatusBar(rt_mat_gen);
    statusBar->setObjectName(QStringLiteral("statusBar"));
    rt_mat_gen->setStatusBar(statusBar);

    retranslateUi(rt_mat_gen);

    QMetaObject::connectSlotsByName(rt_mat_gen);
  } // setupUi

  void retranslateUi(QMainWindow* rt_mat_gen)
  {
    rt_mat_gen->setWindowTitle(
        QApplication::translate("rt_mat_gen", "rt_mat_gen", 0));
    roll_label->setText(QApplication::translate("rt_mat_gen", "Roll", 0));
    pitch_label->setText(QApplication::translate("rt_mat_gen", "Pitch", 0));
    yaw_label->setText(QApplication::translate("rt_mat_gen", "Yaw", 0));
    x_label->setText(QApplication::translate("rt_mat_gen", "X", 0));
    y_label->setText(QApplication::translate("rt_mat_gen", "Y", 0));
    z_label->setText(QApplication::translate("rt_mat_gen", "Z", 0));
  } // retranslateUi
};

namespace Ui
{
class rt_mat_gen : public Ui_rt_mat_gen
{
};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RT_MAT_GEN_H
