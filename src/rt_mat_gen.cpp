#include "rt_mat_generator/rt_mat_gen.h"
#include "ui_rt_mat_gen.h"

rt_mat_gen::rt_mat_gen(int argc, char** argv, QWidget* parent)
  : QMainWindow(parent), ui(new Ui::rt_mat_gen)
{
  ui->setupUi(this);
  ui->x_lineEdit->setText(QString::number(0));
  ui->y_lineEdit->setText(QString::number(0));
  ui->z_lineEdit->setText(QString::number(0));
  ui->roll_lineEdit->setText(QString::number(0));
  ui->pitch_lineEdit->setText(QString::number(0));
  ui->yaw_lineEdit->setText(QString::number(0));

  ros_node = new rt_mat_generator_ui::rt_mat_generator(argc, argv);

  if (!ros_node->init())
    ROS_INFO("ROS node init fail");

  connect(ros_node, SIGNAL(rosShutdown()), qApp, SLOT(quit()));
}

rt_mat_gen::~rt_mat_gen() { delete ui; }

void rt_mat_gen::on_x_lineEdit_textEdited(const QString &x)
{
  ros_node->init_x_ = x.toFloat();
}

void rt_mat_gen::on_y_lineEdit_textEdited(const QString &y)
{
  ros_node->init_y_ = y.toFloat();
}

void rt_mat_gen::on_z_lineEdit_textEdited(const QString &z)
{
  ros_node->init_z_ = z.toFloat();
}

void rt_mat_gen::on_roll_lineEdit_textEdited(const QString &roll)
{
  ui->roll_horizontalSlider->setValue(roll.toFloat() * 10.0);
  ros_node->roll_ = roll.toFloat();
}

void rt_mat_gen::on_pitch_lineEdit_textEdited(const QString &pitch)
{
  ui->pitch_horizontalSlider->setValue(pitch.toFloat() * 10.0);
  ros_node->pitch_ = pitch.toFloat();
}

void rt_mat_gen::on_yaw_lineEdit_textEdited(const QString &yaw)
{
  ui->yaw_horizontalSlider->setValue(yaw.toFloat() * 10.0);
  ros_node->yaw_ = yaw.toFloat();
}

void rt_mat_gen::on_roll_horizontalSlider_sliderMoved(int roll_position)
{
  ui->roll_lineEdit->setText(QString::number((float)roll_position / 10.0));
  ros_node->roll_ = (float)roll_position / 10.0;
}

void rt_mat_gen::on_pitch_horizontalSlider_sliderMoved(int pitch_position)
{
  ui->pitch_lineEdit->setText(QString::number((float)pitch_position / 10.0));
  ros_node->pitch_ = (float)pitch_position / 10.0;
}

void rt_mat_gen::on_yaw_horizontalSlider_sliderMoved(int yaw_position)
{
  ui->yaw_lineEdit->setText(QString::number((float)yaw_position / 10.0));
  ros_node->yaw_ = (float)yaw_position / 10.0;
}

void rt_mat_gen::on_rt_generate_button_clicked()
{
  ui->rt_result_textEdit->setText(ros_node->final_rt_mat_);
}
