#include "rt_mat_generator/rt_mat_generator_ros.h"
#include <iostream>
#include <sstream>
#include <ros/network.h>
#include <ros/ros.h>

using namespace std;

namespace rt_mat_generator_ui
{

rt_mat_generator::rt_mat_generator(int argc, char** argv)
  : init_argc(argc), init_argv(argv), isConnected(false)
{
  camera_info_ok_ = false;

  init_x_ = init_y_ = init_z_ = 0;
  roll_ = pitch_ = yaw_ = 0;
}

rt_mat_generator::~rt_mat_generator()
{
  if (ros::isStarted())
  {
    ros::shutdown(); // explicitly needed since we use ros::start();
    ros::waitForShutdown();
  }
  delete nh; // deallocate ndoe handle
  wait();
}

bool rt_mat_generator::init()
{
  ros::init(init_argc, init_argv, "rt_mat_generator");
  if (!ros::master::check())
  {
    return false;
  }
  init_nh();
  ros::start(); // explicitly needed since our nodehandle is going out of scope.
  start();
  return true;
}

void rt_mat_generator::init_nh()
{
  nh = new ros::NodeHandle("rt_mat_generator");
  ros::NodeHandle private_node_handle("~");

  string prev_extrinsic, points_src, camera_info_src;
  private_node_handle.param<std::string>("prev_extrinsic", prev_extrinsic, "{path}");
  if(prev_extrinsic != "")
  {
    ROS_INFO("previous extrinsic file path : %s", prev_extrinsic.c_str());

    ifstream ifs(prev_extrinsic.c_str());

    if(ifs.is_open())
    {
      int idx = 0;
      string line;

      while(getline(ifs, line))
      {
        size_t pos = line.find('{');
        if(pos != string::npos)
          line = line.substr(pos + 1, line.size());

        pos = line.find('}');
        if(pos != string::npos)
          line = line.substr(0, pos);

        istringstream extrinsic_line(line);
        string extrinsic_attr;

        while(getline(extrinsic_line, extrinsic_attr, ' '))
        {
          origin_mat[idx] = stod(extrinsic_attr);
          idx++;
        }
      }
    }
    ifs.close();
  }

  private_node_handle.param<std::string>("points_src", points_src, "/velodyne_points");
  ROS_INFO("Subscribe point topic : %s", points_src.c_str());
  private_node_handle.param<std::string>("camera_info_src", camera_info_src, "/camera_info");
  ROS_INFO("Subscribe camera info topic : %s", camera_info_src.c_str());

  point_cloud_sub_ = nh->subscribe(points_src, 1, &rt_mat_generator::pointcloudCallback, this);
  camera_info_sub_ = nh->subscribe(camera_info_src, 1, &rt_mat_generator::camerainfoCallback, this);
  projected_point_pub_ = nh->advertise<keti_msgs::ProjectedPoints>("/points_image_msgs", 1);
}

void rt_mat_generator::run()
{
  ros::Rate loop_rate(10);
  while (ros::ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  }
  std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
  Q_EMIT rosShutdown();
}

pcl::PointXYZ rt_mat_generator::TransformPoint(const pcl::PointXYZI &in_point, const tf::StampedTransform &in_transform)
{
  tf::Vector3 tf_point(in_point.x, in_point.y, in_point.z);
  tf::Vector3 tf_point_t = in_transform * tf_point;
  return pcl::PointXYZ(tf_point_t.x(), tf_point_t.y(), tf_point_t.z());
}

tf::StampedTransform rt_mat_generator::FindTransform()
{
  double angle[3] = {0 + yaw_, -90 + pitch_, 90 + roll_};
//  double angle[3] = {0, 0, 0};
  double sine[3];
  double cosine[3];
  double R[9];

  for(int i = 0; i < 3; i++)
  {
    sine[i] = sin(angle[i] * M_PI / 180);
    cosine[i] = cos(angle[i] * M_PI / 180);
  }

  R[0] = cosine[1] *cosine[0];
  R[1] = sine[2] * sine[1] * cosine[0] - cosine[2] * sine[0];
  R[2] = cosine[2] * sine[1] * cosine[0] + sine[2] * sine[0];
  R[3] = cosine[1] * sine[0];
  R[4] = sine[2] * sine[1] * sine[0] + cosine[2] * cosine[0];
  R[5] = cosine[2] * sine[1] * sine[0] - sine[2] * cosine[0];
  R[6] = -sine[1];
  R[7] = sine[2] * cosine[1];
  R[8] = cosine[2] * cosine[1];

  double Temp[12] = {0, 0, 0, -init_y_,
                     0, 0, 0, -init_z_,
                     0, 0, 0, init_x_};
//  double Temp[12] = {0, 0, 0, -init_x_,
//                     0, 0, 0, -init_y_,
//                     0, 0, 0, -init_z_};

  for(int i = 0; i < 3; i++)
  {
    for(int j = 0; j < 4; j++)
    {
      for(int k = 0; k < 3; k++)
      {
        Temp[i * 4 + j] = Temp[i * 4 + j] + R[i * 3 + k] * origin_mat[k * 4 + j];
      }
    }
  }

  tf::Matrix3x3 rotation_mat;
  tf::Quaternion quaternion;
  double roll, pitch, yaw;
  rotation_mat.setValue(Temp[0], Temp[1], Temp[2], Temp[4], Temp[5], Temp[6], Temp[8], Temp[9], Temp[10]);
  rotation_mat.getRPY(roll, pitch, yaw, 1);
  quaternion.setRPY(roll, pitch, yaw);

  tf::StampedTransform transform;
  transform.setOrigin(tf::Vector3(Temp[3], Temp[7], Temp[11]));
  transform.setRotation(quaternion);

  final_rt_mat_ = "";

  for(int i = 0; i < 12; i++)
  {
    if(i != 0)
      final_rt_mat_ += ", ";

    if(i != 0 && i % 4 == 0)
      final_rt_mat_ += "\n";

    final_rt_mat_ += QString::number(Temp[i]);
  }

  return transform;
}

void rt_mat_generator::camerainfoCallback(const sensor_msgs::CameraInfo::ConstPtr &cam_info)
{
  if(!camera_info_ok_)
  {
    image_size_.width = cam_info->width;
    image_size_.height = cam_info->height;

    fx_ = static_cast<float>(cam_info->K[0]);
    fy_ = static_cast<float>(cam_info->K[4]);
    cx_ = static_cast<float>(cam_info->K[2]);
    cy_ = static_cast<float>(cam_info->K[5]);

    std::cout << image_size_.width << ", " << image_size_.height << std::endl;
    std::cout << fx_ << ", " << fy_ << ", " << cx_ << ", " << cy_ << std::endl;

    camera_info_ok_ = true;
  }
  else
    camera_info_sub_.shutdown();
}

void rt_mat_generator::pointcloudCallback(const sensor_msgs::PointCloud2::ConstPtr &points)
{
  camera_lidar_tf_ = FindTransform();

  if(camera_info_ok_)
  {
    pcl::PointCloud<pcl::PointXYZI>::Ptr in_cloud(new pcl::PointCloud<pcl::PointXYZI>);
    pcl::fromROSMsg(*points, *in_cloud);

    std::vector<pcl::PointXYZI> in_pt;
    pcl::PointCloud<pcl::PointXYZI>::Ptr final_pt(new pcl::PointCloud<pcl::PointXYZI>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr out_detected_box_pt(new pcl::PointCloud<pcl::PointXYZ>);
    final_pt->header = out_detected_box_pt->header = in_cloud->header;
    std::vector<cv::Point2d> pt_in_image;

    keti_msgs::ProjectedPoints point_msgs;
    point_msgs.header = points->header;
    point_msgs.image_width = image_size_.width;
    point_msgs.image_height = image_size_.height;

    point_msgs.distance = std::vector<float>(image_size_.height * image_size_.width, 0);

    // extract points inside image frame
    for(size_t i = 0; i < in_cloud->points.size(); i++)
    {
//      if(in_cloud->points[i]._PointXYZI::z < -1.7) continue;

      pcl::PointXYZ cam_cloud = TransformPoint(in_cloud->points[i], camera_lidar_tf_);
      int u = int(cam_cloud.x * fx_ / cam_cloud.z + cx_);
      int v = int(cam_cloud.y * fy_ / cam_cloud.z + cy_);

      if((u >= 0) && (u < image_size_.width) && (v >= 0) && (v < image_size_.height) && cam_cloud.z > 0)
      {
        cv::Point2d pt;
        pt.x = u;
        pt.y = v;
        pt_in_image.push_back(pt);
        in_pt.push_back(in_cloud->points[i]);

        point_msgs.distance[(image_size_.width * v) + u] = std::sqrt((in_cloud->points[i]._PointXYZI::x * in_cloud->points[i]._PointXYZI::x)
                                                                     + (in_cloud->points[i]._PointXYZI::y * in_cloud->points[i]._PointXYZI::y));
      }
    }

    projected_point_pub_.publish(point_msgs);
  }
  else
    ROS_INFO("Camera info not found");
}

} // namespace rt_mat_generator_ui
