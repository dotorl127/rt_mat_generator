#include "rt_mat_generator/rt_mat_gen.h"
#include <QApplication>

int main(int argc, char* argv[])
{
  QApplication a(argc, argv);
  rt_mat_gen w(argc, argv);
  w.show();

  return a.exec();
}
